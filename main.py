#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

# Turn list of books into JSON format
@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)

# Main page, shows all of the books with options to either edit or delete them
@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
# Add a new book
# Looks if 'id' exists in list of books
# If not, then append book
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        key = request.form['name']
        key = str(key)
        new_id = 1
        index = 0
        new_book = dict()
        while index < len(books):
            if (books[index]['id'] != str(new_id)):
                new_book = {'title': key, 'id': str(new_id)}
                books.append(new_book)
                return redirect(url_for('showBook', books=books))
            new_id += 1
            index += 1
        new_book = {'title': key, 'id': str(new_id)}
        books.append(new_book)
        return redirect(url_for('showBook', books=books))
    else:
        return render_template('newBook.html')    

# Edits current book
# Looks for 'id' of book and changes the title based on user input
@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        key = request.form['name']
        key = str(key)
        book_id = str(book_id)
        # Checks list of books for specific id
        for i in range(len(books)):
            if books[i]['id'] == book_id:
                books[i]['title'] = key
        return redirect(url_for('showBook', books=books))
    else:
        return render_template('editBook.html', book_id=book_id)

# Deletes current book
# Similar to editBook, checks through list for 'id' of book and deletes it
@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    index = 0
    book_title = ''
    for i in range(len(books)):
            if books[i]['id'] == str(book_id):
                index = i
                book_title = books[i]['title']
    if request.method == 'POST':
        del books[index]
        return redirect(url_for('showBook', books=books))
    else:
        return render_template('deleteBook.html', book_title=books[index]['title'], book_id=book_id)
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

